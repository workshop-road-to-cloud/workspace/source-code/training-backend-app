package com.cloudtraining;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class NoteApiApplication {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(NoteApiApplication.class);
		application.setBannerMode(Banner.Mode.OFF);
		application.run(args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("http://ec2-52-87-165-224.compute-1.amazonaws.com/");
			}
		};
	}
}
