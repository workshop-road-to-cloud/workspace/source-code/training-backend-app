package com.cloudtraining.adapter.persistence.out.mysql;

import com.cloudtraining.adapter.persistence.out.mysql.dao.NoteDao;
import com.cloudtraining.core.application.port.out.NotePort;
import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
@Transactional(
    propagation = Propagation.REQUIRED, readOnly = true,
    rollbackFor = {Exception.class}, isolation = Isolation.READ_COMMITTED)
public class NotePersistenceAdapter implements NotePort {

  private final NoteDao noteDao;

  @Override
  public List<NoteDomain> findNotes() {
    return noteDao.findAll();
  }

  @Override
  public Optional<NoteDomain> findNote(Long noteId) {
    return noteDao.findNote(noteId);
  }
}
