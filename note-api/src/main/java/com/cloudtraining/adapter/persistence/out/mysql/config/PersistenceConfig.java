package com.cloudtraining.adapter.persistence.out.mysql.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.cloudtraining.adapter.persistence.out.mysql.dao.repository")
@EntityScan(PersistenceConfig.ENTITIES_PACKAGE)
public class PersistenceConfig {
  public static final String ENTITIES_PACKAGE = "com.cloudtraining.adapter.persistence.out.mysql.dao.entity";
}
