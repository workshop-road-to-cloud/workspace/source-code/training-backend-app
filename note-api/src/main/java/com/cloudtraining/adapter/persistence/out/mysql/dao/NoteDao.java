package com.cloudtraining.adapter.persistence.out.mysql.dao;

import com.cloudtraining.adapter.persistence.out.mysql.dao.repository.NoteRepository;
import com.cloudtraining.adapter.persistence.out.mysql.util.EntityToDomainMapper;
import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class NoteDao {

  private final NoteRepository noteRepository;

  public List<NoteDomain> findAll() {
    return noteRepository.findAll().stream()
        .map(note -> EntityToDomainMapper.applyMapping(note))
        .collect(Collectors.toList());
  }

  public Optional<NoteDomain> findNote(Long noteId) {
    return noteRepository.findById(noteId)
        .map(note -> EntityToDomainMapper.applyMapping(note));
  }
}
