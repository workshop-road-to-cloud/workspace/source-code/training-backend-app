package com.cloudtraining.adapter.persistence.out.mysql.dao.entity.base;

import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
@Data
public abstract class AuditableEntity implements Serializable {

  /**
   * When the entity class was created
   */
  @CreatedDate
  private Date createdDate;

  /**
   * Indicates when was the last date the entity class was changed
   */
  @LastModifiedDate
  private Date lastModifiedDate;

  /**
   * Determines the number of times an entity class has changed over time
   */
  @Version
  private Integer version;
}
