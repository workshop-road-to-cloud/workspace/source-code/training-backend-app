package com.cloudtraining.adapter.persistence.out.mysql.dao.repository;

import com.cloudtraining.adapter.persistence.out.mysql.dao.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {

}
