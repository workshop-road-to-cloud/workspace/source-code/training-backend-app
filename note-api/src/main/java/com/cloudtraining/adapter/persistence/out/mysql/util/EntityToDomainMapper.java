package com.cloudtraining.adapter.persistence.out.mysql.util;

import com.cloudtraining.adapter.persistence.out.mysql.dao.entity.Note;
import com.cloudtraining.core.domain.model.NoteDomain;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EntityToDomainMapper {

  private static final SimpleDateFormat DATE_FORMAT_YYY_MM_DD =
      new SimpleDateFormat("yyyy-MM-dd");

  public static NoteDomain applyMapping(Note note) {
    if(note == null) {
      return null;
    }

    return NoteDomain.builder()
        .id(note.getId())
        .title(note.getTitle())
        .body(note.getBody())
        .lastUpdatedDate(getAsString(note.getLastModifiedDate()))
        .build();
  }

  private static String getAsString(Date date) {
    return DATE_FORMAT_YYY_MM_DD.format(date);
  }
}