package com.cloudtraining.adapter.rest.in.api;

import com.cloudtraining.adapter.rest.in.dto.NoteDto;
import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface NoteRestApi {

  @GetMapping("/notes")
  List<NoteDto> findNotes();

  @GetMapping("/notes/{noteId}")
  Optional<NoteDto> findNoteDetail(
      @PathVariable(value = "noteId") Long noteId);
}
