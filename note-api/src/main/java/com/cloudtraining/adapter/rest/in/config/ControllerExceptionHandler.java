package com.cloudtraining.adapter.rest.in.config;

import static java.lang.String.join;
import static java.util.stream.Collectors.toList;

import com.cloudtraining.adapter.rest.in.dto.ResponseStatusDto;
import com.cloudtraining.core.domain.exception.DomainErrorLevel;
import com.cloudtraining.core.domain.exception.DomainException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@ControllerAdvice(annotations = RestController.class)
public class ControllerExceptionHandler {

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ResponseStatusDto> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException exception) {
    log.warn("[MethodArgumentNotValidException]", exception);
    List<String> allErrors = exception.getBindingResult().getAllErrors().stream()
        .filter(objectError -> objectError instanceof FieldError)
        .map(objectError -> {
          FieldError fieldWithError = ((FieldError) objectError);
          return "'" + fieldWithError.getField() + "' " + fieldWithError.getDefaultMessage();
        })
        .collect(toList());

    return ResponseStatusDto.createResponse(
        HttpStatus.BAD_REQUEST, DomainErrorLevel.ERROR,
        "Fields with error: " + join(", ", allErrors));
  }

  /**
   * This is to manage the error in case the client make a request with the correct endpoint url but
   * with incorrect method
   */
  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  public ResponseEntity<ResponseStatusDto> handleHttpMediaTypeNotSupportedException(
      HttpMediaTypeNotSupportedException exception) {
    log.warn("HttpMediaTypeNotSupportedException", exception);

    return ResponseStatusDto.createResponse(
        HttpStatus.BAD_REQUEST, DomainErrorLevel.ERROR,
        ExceptionUtils.getRootCauseMessage(exception));
  }

  //TODO Mapp all the possible Domain Exceptions to an specific type of response
  @ExceptionHandler(DomainException.class)
  public ResponseEntity<ResponseStatusDto> handleDomainException(
      DomainException domainException) {
    log.warn("[DomainException]", domainException);

    //Manage the HTTP Status depending on the DomainException
    switch (domainException.getError()) {
      case INSUFFICIENT_PERMISSIONS:
        return ResponseStatusDto.createResponse(
            HttpStatus.UNAUTHORIZED, domainException.getErrorLevel(),
            domainException.getMessage());
      default:
        return ResponseStatusDto.createResponse(
            HttpStatus.BAD_REQUEST, domainException.getErrorLevel(),
            domainException.getMessage());
    }
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ResponseStatusDto> handleAllExceptions(Exception ex) {
    log.error("Unexpected Exception:", ex);
    return ResponseStatusDto.createResponse(
        HttpStatus.INTERNAL_SERVER_ERROR, DomainErrorLevel.ERROR,
        ExceptionUtils.getRootCauseMessage(ex));
  }
}