package com.cloudtraining.adapter.rest.in.controller;

import com.cloudtraining.adapter.rest.in.api.NoteRestApi;
import com.cloudtraining.adapter.rest.in.dto.NoteDto;
import com.cloudtraining.adapter.rest.in.util.DomainToDtoMapper;
import com.cloudtraining.core.application.port.in.FindNoteDetailUseCasePort;
import com.cloudtraining.core.application.port.in.FindNotesUseCasePort;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
public class NoteRestControllerAdapter implements NoteRestApi {

  private final FindNotesUseCasePort findNotesUseCasePort;
  private final FindNoteDetailUseCasePort findNoteDetailUseCasePort;

  @Override
  public List<NoteDto> findNotes() {
    return findNotesUseCasePort.findNotes().stream()
        .map(noteDomain -> DomainToDtoMapper.applyMapping(noteDomain))
        .collect(Collectors.toList());
  }

  @Override
  public Optional<NoteDto> findNoteDetail(Long noteId) {
    return findNoteDetailUseCasePort.findNoteDetail(noteId)
        .map(noteDomain -> DomainToDtoMapper.applyMapping(noteDomain));
  }
}
