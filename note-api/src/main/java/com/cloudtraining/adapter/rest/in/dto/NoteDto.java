package com.cloudtraining.adapter.rest.in.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Data
@NoArgsConstructor
public class NoteDto {

  private Long id;
  private String title;
  private String bodyMinimal;
  private String body;
  private String lastUpdatedDate;
}
