package com.cloudtraining.adapter.rest.in.dto;

import com.cloudtraining.core.domain.exception.DomainErrorLevel;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Getter
@Builder
public class ResponseStatusDto {

  private final Long timestamp;
  private final int status;
  private final String message;

  private final String level;

  public static ResponseEntity<ResponseStatusDto> createResponse(
      HttpStatus httpStatus, DomainErrorLevel errorLevel, String message) {
    return new ResponseEntity<>(
        ResponseStatusDto.builder()
            .timestamp(System.currentTimeMillis())
            .status(httpStatus.value())
            .message(message)
            .level(errorLevel.name())
            .build(), httpStatus);
  }
}