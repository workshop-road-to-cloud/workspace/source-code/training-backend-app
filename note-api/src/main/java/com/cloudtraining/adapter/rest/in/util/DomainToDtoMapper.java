package com.cloudtraining.adapter.rest.in.util;

import com.cloudtraining.adapter.rest.in.dto.NoteDto;
import com.cloudtraining.core.domain.model.NoteDomain;

public class DomainToDtoMapper {

  public static NoteDto applyMapping(NoteDomain noteDomain) {
    if(noteDomain == null){
      return null;
    }
    NoteDto noteDto = new NoteDto();
    noteDto.setId(noteDomain.getId());
    noteDto.setTitle(noteDomain.getTitle());
    noteDto.setBodyMinimal(noteDomain.getBodyMinimal());
    noteDto.setBody(noteDomain.getBody());
    noteDto.setLastUpdatedDate(noteDomain.getLastUpdatedDate());
    return noteDto;
  }
}
