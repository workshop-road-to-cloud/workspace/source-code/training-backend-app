package com.cloudtraining.assembly;

import com.cloudtraining.core.application.port.in.FindNoteDetailUseCasePort;
import com.cloudtraining.core.application.port.in.FindNotesUseCasePort;
import com.cloudtraining.core.application.port.out.NotePort;
import com.cloudtraining.core.application.usecase.FindNoteDetailUseCase;
import com.cloudtraining.core.application.usecase.FindNotesUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.cloudtraining.adapter")
@RequiredArgsConstructor
public class NoteContext {

  @Bean
  public FindNotesUseCasePort createFindNotesUseCase(
      NotePort notePort,
      @Value("${configuration.body-minimal.max-length}") Integer bodyMinimalMaxLength) {
    return new FindNotesUseCase(notePort, bodyMinimalMaxLength);
  }

  @Bean
  public FindNoteDetailUseCasePort createFindNoteDetailUseCase(NotePort notePort) {
    return new FindNoteDetailUseCase(notePort);
  }
}
