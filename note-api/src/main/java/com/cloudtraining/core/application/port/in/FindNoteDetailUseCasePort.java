package com.cloudtraining.core.application.port.in;

import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.Optional;

public interface FindNoteDetailUseCasePort {

  Optional<NoteDomain> findNoteDetail(Long noteId);
}
