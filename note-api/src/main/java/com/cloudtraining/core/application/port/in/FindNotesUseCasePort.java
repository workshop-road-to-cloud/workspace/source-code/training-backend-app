package com.cloudtraining.core.application.port.in;

import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.List;

public interface FindNotesUseCasePort {

  List<NoteDomain> findNotes();
}
