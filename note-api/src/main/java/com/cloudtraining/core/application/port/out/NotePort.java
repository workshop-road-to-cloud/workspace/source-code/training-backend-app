package com.cloudtraining.core.application.port.out;

import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.List;
import java.util.Optional;

public interface NotePort {

  List<NoteDomain> findNotes();

  Optional<NoteDomain> findNote(Long noteId);
}
