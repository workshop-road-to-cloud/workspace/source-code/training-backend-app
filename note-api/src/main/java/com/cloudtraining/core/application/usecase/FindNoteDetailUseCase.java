package com.cloudtraining.core.application.usecase;

import com.cloudtraining.core.application.port.in.FindNoteDetailUseCasePort;
import com.cloudtraining.core.application.port.out.NotePort;
import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class FindNoteDetailUseCase implements FindNoteDetailUseCasePort {

  private final NotePort notePort;


  @Override
  public Optional<NoteDomain> findNoteDetail(Long noteId) {
    return notePort.findNote(noteId);
  }
}
