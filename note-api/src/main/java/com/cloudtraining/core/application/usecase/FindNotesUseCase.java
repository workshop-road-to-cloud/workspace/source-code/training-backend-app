package com.cloudtraining.core.application.usecase;

import com.cloudtraining.core.application.port.in.FindNotesUseCasePort;
import com.cloudtraining.core.application.port.out.NotePort;
import com.cloudtraining.core.domain.model.NoteDomain;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@RequiredArgsConstructor
public class FindNotesUseCase implements FindNotesUseCasePort {

  private final NotePort notePort;
  private final Integer bodyMinimalMaxLength;

  @Override
  public List<NoteDomain> findNotes() {
    return notePort.findNotes().stream()
        .map(noteDomain -> {
          noteDomain.setBodyMinimal(getSubstring(noteDomain.getBody(), bodyMinimalMaxLength));
          noteDomain.setBody(null);
          return noteDomain;
        })
        .collect(Collectors.toList());
  }

  private String getSubstring(String originalString, Integer subStringLength) {
    if(StringUtils.isEmpty(originalString)) {
      return null;
    }
    if(originalString.length() <= subStringLength) {
      return originalString;
    }
    return originalString.substring(0, subStringLength);
  };
}
