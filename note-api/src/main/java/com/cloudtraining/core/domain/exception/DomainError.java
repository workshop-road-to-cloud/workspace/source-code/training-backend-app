package com.cloudtraining.core.domain.exception;

public enum DomainError {
  MISSED_REQUIRED_VALUE, INVALID_VALUE, INSUFFICIENT_PERMISSIONS, UNHANDLED
}
