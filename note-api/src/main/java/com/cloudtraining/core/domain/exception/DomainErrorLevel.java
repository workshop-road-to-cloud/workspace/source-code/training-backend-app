package com.cloudtraining.core.domain.exception;

public enum DomainErrorLevel {
  INFO, WARNING, ERROR
}
