package com.cloudtraining.core.domain.exception;

import lombok.Data;

@Data
public class DomainException extends RuntimeException {

  private final DomainError error;
  private final DomainErrorLevel errorLevel;
  private final String message;
}