package com.cloudtraining.core.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NoteDomain {

  private final Long id;
  private final String title;
  private String body;
  private String bodyMinimal;
  private final String lastUpdatedDate;
}
